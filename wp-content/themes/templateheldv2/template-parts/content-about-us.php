<?php
/**
 * Template part for displaying page content in page-about-us.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>


<?php
    $anchor = get_field('link-anchor', $post->ID);
    $intro = get_field('text', $post->ID);
    $members = get_field('members', $post->ID);
    shuffle($members);
    $products = get_field('products', $post->ID);
?>

<section id="<?php echo $anchor; ?>">
  <div class="container-custom animated animated-in bottom-to-top fade">
    <div class="row">
      <header class="col-12 col-md">
        <h2 class="h1"><?php echo get_the_title(); ?></h2>
      </header>
      <?php if (!empty($intro)): ?>
        <div class="col-12 col-md-auto">
          <?php echo $intro; ?>
        </div>
      <?php endif; ?>
    </div>
    <?php if (!empty($members)): ?>
      <div class="row">
        <div class="content-gallery">
          <div class="row">
            <?php
              foreach ($members as $member):
                $member_name = $member['member_name'];
                $member_title = $member['member_title'];
                $member_details = $member['member_details'];
                $member_image = $member['member_image'];
                $member_image_src = wp_get_attachment_image_src( $member_image, 'full' )[0];
                $member_image_srcset = wp_get_attachment_image_srcset( $member_image, 'full' );
                $member_image_sizes = wp_get_attachment_image_sizes( $member_image, 'full' );
                $member_image_alt = get_post_meta( $member_image, '_wp_attachment_image_alt', true);
            ?>
              <?php if ($member_image): ?>
                <figure>
                  <div class="wrapper">
                    <img src="<?php echo esc_attr( $member_image_src );?>"
                    srcset="<?php echo esc_attr( $member_image_srcset ); ?>"
                    sizes="<?php echo esc_attr( $member_image_sizes );?>"
                    alt="<?php echo esc_attr( $member_image_alt );?>">
                    <div class="overlay">
                      <?php echo $member_details; ?>
                    </div>
                  </div>
                  <figcaption>
                    <strong><?php echo $member_name; ?></strong><br>
                    <?php echo $member_title; ?>
                  </figcaption>
                </figure>
              <?php endif; ?>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</section>
