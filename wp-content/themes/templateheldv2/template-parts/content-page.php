<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>


<section id="<?php echo strtolower(urlencode($post->post_title))?>">
  <header>
    <h2><?php echo get_the_title(); ?></h2>
  </header>

  <div class="container">
    <?php the_content(); ?>
  </div>

</section>
