<?php
/**
 * Template part for displaying page content in page-what-we-do.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<?php
  $case_studies = get_field('case_studies', $post->ID);
?>

<?php if (!empty($case_studies)): ?>

  <?php
    foreach ($case_studies as $case_study):
      $case_title = $case_study['case_title'];
      $case_text = $case_study['case_text'];
      $case_anchor = str_replace(' ', '-', strtolower($case_title));
      $case_image = $case_study['case_image_desktop'];
      $case_image_title = get_the_title($case_image);
      $case_image_src = wp_get_attachment_image_src( $case_image, 'full' )[0];
      $case_image_srcset = wp_get_attachment_image_srcset( $case_image, 'full' );
      $case_image_sizes = wp_get_attachment_image_sizes( $case_image, 'full' );
      $case_image_alt = get_post_meta( $case_image, '_wp_attachment_image_alt', true);
      $case_image_mobile = $case_study['case_image_mobile'];
      $case_image_mobile_title = get_the_title($case_image_mobile);
      $case_image_mobile_src = wp_get_attachment_image_src( $case_image_mobile, 'full' )[0];
      $case_image_mobile_srcset = wp_get_attachment_image_srcset( $case_image_mobile, 'full' );
      $case_image_mobile_sizes = wp_get_attachment_image_sizes( $case_image_mobile, 'full' );
      $case_image_mobile_alt = get_post_meta( $case_image_mobile, '_wp_attachment_image_alt', true);
  ?>

    <section id="<?php echo $case_anchor; ?>" class="case-studies">
      <div class="container-custom animated animated-in bottom-to-top fade">
        <div class="row">
          <header class="col-12">
            <h2 class="h1"><?php echo $case_title; ?></h2>
          </header>
          <div class="case-text">
            <?php echo $case_text; ?>
          </div>
        </div>
        <?php if ($case_image): ?>
          <div class="row">
            <div class="content-gallery">
              <div class="row">
                <figure class="desktop">
                  <div class="wrapper">
                    <img src="<?php echo esc_attr( $case_image_src );?>"
                    srcset="<?php echo esc_attr( $case_image_srcset ); ?>"
                    sizes="<?php echo esc_attr( $case_image_sizes );?>"
                    alt="<?php echo esc_attr( $case_image_alt );?>">
                  </div>
                  <figcaption>
                    <strong><?php echo $case_image_title; ?></strong><br>
                    <?php echo $case_image_alt; ?>
                  </figcaption>
                </figure>
                <?php if ($case_image_mobile): ?>
                  <figure class="mobile">
                    <div class="wrapper">
                      <img src="<?php echo esc_attr( $case_image_mobile_src );?>"
                      srcset="<?php echo esc_attr( $case_image_mobile_srcset ); ?>"
                      sizes="<?php echo esc_attr( $case_image_mobile_sizes );?>"
                      alt="<?php echo esc_attr( $case_image_mobile_alt );?>">
                    </div>
                    <figcaption>
                      <strong><?php echo $case_image_mobile_title; ?></strong><br>
                      <?php echo $case_image_mobile_alt; ?>
                    </figcaption>
                  </figure>
                <?php endif; ?>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </section>

  <?php endforeach; ?>
<?php endif; ?>
