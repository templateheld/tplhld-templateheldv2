<?php
/**
* Template part for displaying page content in page-home.php
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package Templateheld
*/

?>

<?php
  // Use WP loop, but for child pages
  // Src: https://wordpress.stackexchange.com/questions/93844/child-pages-loop

  $slides = array(
    'post_parent' => $post->ID,
    'post_type' => 'page',
    'orderby' => 'menu_order',
    'order' => 'ASC'
  );

  $slides = new WP_Query( $slides );
?>

<?php while ( $slides->have_posts() ) : $slides->the_post(); ?>

  <?php
    $slide_template = str_replace('.php', '', str_replace('page-', '', get_page_template_slug($post->ID)));
    get_template_part( 'template-parts/content', $slide_template );
  ?>

<?php endwhile; ?>

<?php
  wp_reset_postdata();
