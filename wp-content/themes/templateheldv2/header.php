<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/safari-pinned-tab.svg" color="#000000">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header id="header">
		<div class="container-custom">
			<div class="row">
				<div id="logo">
					<a id="back-to-top" href="<?php echo home_url(); ?>#start">
						<img src="<?php echo get_template_directory_uri() . '/img/logo-white.svg' ?>" alt="<?php echo get_bloginfo('title') . ' – ' . get_bloginfo('description'); ?>">
					</a>
				</div>
				<nav id="menu-primary-container">
					<?php
						$main_nav = array(
							'menu' => 'primary',
							'menu_class' => ' ',
							'container' => ''
						);

						wp_nav_menu($main_nav);
					?>
				</nav>
			</div>
		</div>
	</header>
	<section id="intro" class="animated animated-in fade">
		<img src="<?php echo get_template_directory_uri() . '/img/logo-white.svg' ?>" alt="<?php echo get_bloginfo('title') . ' – ' . get_bloginfo('description'); ?>">
	</section>
	<section id="start">
		<div id="video-container">
			<video id="video" autoplay loop muted playsinline>
				<source src="<?php echo get_template_directory_uri() . '/assets/eonm-intro.mp4' ?>" type="video/mp4">
				<source src="<?php echo get_template_directory_uri() . '/assets/eonm-intro.webm' ?>" type="video/webm">
				<img src="<?php echo get_template_directory_uri() . '/img/bg-video.png' ?>" alt="">
			</video>

		</div>
		<div class="container-custom">
			<div class="row">
				<div class="col-12 col-xs animated animated-out animated-in fade bottom-to-top">
					<h1><?php echo get_bloginfo('description'); ?></h1>
				</div>
				<div class="col-12 col-xs animated animated-out animated-in fade bottom-to-top">
					<p>
						<a href="tel:+4982134322423">+ 49 821 34 32 24 - 23</a><br>
						<a href="mailto:info@eonm.de">info@eonm.de</a>
					</p>
				</div>
			</div>
		</div>
	</section>
