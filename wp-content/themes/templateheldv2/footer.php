<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

?>

<footer id="footer">
  <div class="container-custom">
    <div class="row">
      <header class="col-12 col-xs">
        <h2 class="h1"><?php echo get_bloginfo('description'); ?></h2>
      </header>
      <div class="col-12 col-xs">
        <p>
          <a href="tel:+4982134322423">+ 49 821 34 32 24 - 23</a><br>
          <a href="mailto:info@eonm.de">info@eonm.de</a>
        </p>
      </div>
    </div>
    <div class="row">
      <address class="col">
        <p>
          EONM<br>
          Johannes-Haag-Straße 3<br>
          86163 Augsburg
        </p>
      </address>
      <nav id="menu-footer-container">
        <?php
          $main_nav = array(
            'menu' => 'footer',
            'menu_class' => ' ',
            'container' => ''
          );

          wp_nav_menu($main_nav);
        ?>
      </nav>
    </div>
  </div>
</footer>

<?php
  $frontpage_id = get_option('page_on_front');
  $modals = get_field('onpage_modals', $frontpage_id);
?>

<?php if (!empty($modals)): ?>

  <div id="modals">
    <?php
      foreach ($modals as $modal):
        $modal_title = $modal['modal_title'];
        $modal_text = $modal['modal_text'];
        $modal_anchor = str_replace(' ', '-', strtolower($modal_title));
    ?>

      <section id="<?php echo $modal_anchor; ?>" class="modal">
        <div class="container-custom">
          <div class="row">
            <header>
              <h2 class="h1"><?php echo $modal_title; ?></h2>
            </header>
            <div class="content-text">
              <?php echo $modal_text; ?>
            </div>
          </div>
        </div>
      </section>

    <?php endforeach; ?>
  </div>
  
<?php endif; ?>


<?php wp_footer(); ?>

</body>
</html>
