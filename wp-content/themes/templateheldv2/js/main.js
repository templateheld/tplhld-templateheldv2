// Classes
var IntroAnim = function() {
  this.elems = {
    intro: '#intro',
    body: 'body',
    scroll: {
      page: 'html, body',
      toElem: '#header'
    }
  },
  this.states = {
    finished: 'intro-finished',
    scrollFinished: 'scroll-finished',
    scrollStarted: 'scroll-started'
  },
  this.options = {
    fade: {
      delay: 3000,
      duration: 1000,
      easing: 'swing'
    },
    scroll: {
      delay: -300,
      duration: 1300,
      easing: 'easeOutCubic'
    }
  },
  this.breakpoints = {
    xs: 500,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200
  },
  this.getBottom = function(elem) {
    return Math.floor(jQuery(elem).outerHeight());
  },
  this.animate = function() {
    var introAnim = this;

    setTimeout(function() {
      jQuery(introAnim.elems.intro).fadeOut(introAnim.options.fade.duration, function() {
        jQuery(introAnim.elems.body).addClass(introAnim.states.finished);
      })
    }, introAnim.options.fade.delay);
  },
  this.scroll = function(scrollTo, delay) {
    var introAnim = this;

    setTimeout(function() {
      // Set scroll-finished class on intro scroll start
      jQuery(introAnim.elems.body).addClass(introAnim.states.scrollStarted);

      jQuery(introAnim.elems.scroll.page).animate({
        scrollTop: scrollTo
      }, introAnim.options.scroll.duration, introAnim.options.scroll.easing, function() {

        // Set scroll-finished class on intro scroll finish
        jQuery(introAnim.elems.body).addClass(introAnim.states.scrollFinished);
      })
    }, delay);
  },
  this.resetScroll = function() {
    setTimeout(function() {
      window.scrollTo({
        top: 0,
        behavior: 'instant'
      });
    }, 100)
  },
  this.init = function() {
    var elemBottom = this.getBottom(this.elems.scroll.toElem);

    this.resetScroll();
    this.animate();
    if (jQuery(window).width() <= this.breakpoints.xs) {
      this.scroll(0, 0);
    } else {
      this.scroll(elemBottom, this.options.fade.delay + this.options.scroll.delay);
    }
  }
}

var TabNavProducts = function() {
  this.elems = {
    nav: '.content-products nav button',
    tabs: '.row'
  },
  this.states = {
    active: 'active'
  },
  this.init = function() {
    var tabNav = this,
        target;

    jQuery(this.elems.nav).each(function(){

      jQuery(this).on('click', function() {
        target = jQuery(this).attr('data-toggle');

        // jQuery(target).siblings().removeClass(tabNav.states.active);
        // jQuery(target).addClass(tabNav.states.active);

        jQuery(target).siblings(tabNav.elems.tabs).fadeOut(300, function() {
          jQuery(this).removeClass(tabNav.states.active);
          jQuery(target).addClass(tabNav.states.active);
          jQuery(target).fadeIn(300);
        });

        jQuery(this).siblings().removeClass(tabNav.states.active);
        jQuery(this).addClass(tabNav.states.active);
      })
    })
  }
}

var VideoAutoplay = function() {
  this.elems = {
    video: 'video',
  },
  this.mute = function(elem) {
    video.muted = true;
  },
  this.play = function(elem) {
    // Mute video
    this.mute(video);

    video.addEventListener('loadeddata', function() {
      if(video.readyState >= 2) {
        video.play();
      }
    });
  },
  this.init = function() {
    var video = document.getElementById(this.elems.video);

    this.play(video);
  }
}

var AnimationStates = function() {
  this.stickyNav = new StickyNav(),
  this.elems = {
    body: 'body',
    header: '#header'
  },
  this.states = {
    video: 'video-view'
  },
  this.getBottom = function(elem) {
    return jQuery(elem).offset().top + jQuery(elem).outerHeight() - window.innerHeight;
  },
  this.getTop = function(elem) {
    return jQuery(elem).offset().top - window.innerHeight;
  },
  this.getHeight = function(elem) {
    return jQuery(elem).outerHeight();
  },
  this.triggerVideoAnim = function() {
    jQuery(this.elems.body).addClass(this.states.video);
  },
  this.resetVideoAnim = function() {
    jQuery(this.elems.body).removeClass(this.states.video);
  }
  this.init = function() {
    var animationStates = this,
        currentPos,
        triggerVideoPos = this.getTop(this.elems.header) + (this.getHeight(this.elems.header) / 2);

    jQuery(window).on('resize', function() {
      // Unsticky nav to get correct offset
      // Stickied nav messes .offset().top calculations up
      animationStates.stickyNav.unsticky();

      triggerVideoPos = animationStates.getTop(animationStates.elems.header) + (animationStates.getHeight(animationStates.elems.header) / 2);

      animationStates.stickyNav.sticky();
    });

    jQuery(window).on('scroll resize', function() {
      currentPos = jQuery(window).scrollTop();

      // Trigger Video anim
      if (currentPos <= triggerVideoPos) {
        animationStates.triggerVideoAnim();
      } else {
        animationStates.resetVideoAnim();
      }
    })
  }
}

var StickyNav = function() {
  this.elems = {
    nav: '#header',
    body: 'body'
  },
  this.states = {
    sticky: 'sticky-nav',
    minify: 'mini-nav'
  },
  this.vars = {
    fontSize: 16,
    navSizeDiffInEm: 5,
    navSizeDiffInEmSm: 7.5,
    vwSize: 4,
    vwSizeXs: 2,
    vwSizeSm: 1.5,
    vwSizeLg: 1.25,
    vwSizeXl: 1
  },
  this.breakpoints = {
    xs: 500,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200
  },
  this.navSizeDiff = function() {
    var navSizeDiff = this.vars.navSizeDiffInEmXs,
        vw = jQuery(window).width() / 100,
        rem = vw * this.vars.vwSize,
        em;

    if (jQuery(window).width() >= this.breakpoints.xs && jQuery(window).width() < this.breakpoints.sm){
      navSizeDiff = this.vars.navSizeDiffInEmXs;
      rem = vw * this.vars.vwSizeXs;
    }

    if (jQuery(window).width() >= this.breakpoints.sm){
      navSizeDiff = this.vars.navSizeDiffInEmSm;
      rem = vw * this.vars.vwSizeSm;
    }

    if (jQuery(window).width() >= this.breakpoints.lg && jQuery(window).width() < this.breakpoints.xl){
      rem = vw * this.vars.vwSizeLg;
    }

    if (jQuery(window).width() >= this.breakpoints.xl){
      rem = vw * this.vars.vwSizeXl;
    }

    em = rem / 10 / 100 * 52.083325 * this.vars.fontSize;

    return navSizeDiff * em;
  },
  this.getTop = function(elem) {
    return jQuery(elem).offset().top;
  },
  this.sticky = function() {
    jQuery(this.elems.body).addClass(this.states.sticky);
  },
  this.unsticky = function() {
    jQuery(this.elems.body).removeClass(this.states.sticky);
  },
  this.minify = function() {
    jQuery(this.elems.body).addClass(this.states.minify);
  },
  this.unminify = function() {
    jQuery(this.elems.body).removeClass(this.states.minify);
  },
  this.init = function() {
    var stickyNav = this,
        currentPos,
        navTop,
        triggerMiniNav;

    this.unsticky();

    navTop = this.getTop(this.elems.nav),
    triggerMiniNav = navTop + this.navSizeDiff();

    this.sticky();

    jQuery(window).on('resize', function() {
      // Unsticky nav to get correct offset
      // Stickied nav always has 0px offset
      stickyNav.unsticky();

      navTop = stickyNav.getTop(stickyNav.elems.nav),
      triggerMiniNav = navTop + stickyNav.navSizeDiff();

      stickyNav.sticky();
    });

    jQuery(window).on('scroll resize', function() {
      currentPos = jQuery(window).scrollTop();

      // Trigger sticky nav
      if (currentPos <= navTop) {
        stickyNav.unsticky();
      } else {
        stickyNav.sticky();
      }

      // Trigger mini nav
      if (currentPos <= triggerMiniNav) {
        stickyNav.unminify();
      } else {
        stickyNav.minify();
      }
    })
  }
}

var SmoothScroll = function() {
  this.elems = {
    links: 'a[href*="#"]',
    page: 'html, body',
    exception: '#footer, #logo'
  },
  this.options = {
    duration: 1300,
    easing: 'easeOutCubic'
  }
  this.getTop = function(elem) {
    return Math.floor(jQuery(elem).offset().top);
  },
  this.isException = function(elem) {
    if (jQuery(elem).parents(this.elems.exception).length) {
      return true;
    } else {
      return false;
    }
  },
  this.scroll = function(elem) {
    var smoothScroll = this,
        elemTop,
        target,
        isException;  // Filters out footer nav

    jQuery(elem).on('click', function(e) {
      isException = smoothScroll.isException(elem);

      if (!isException) {
        e.preventDefault();

        target = jQuery(this).attr('href');
        elemTop = smoothScroll.getTop(target);

        jQuery(smoothScroll.elems.page).animate({
          scrollTop: elemTop
        }, smoothScroll.options.duration, smoothScroll.options.easing, function() {
          window.location.hash = target;
        })
      }
    })
  },
  this.init = function() {
    var smoothScroll = this,
        elemTop,
        target;

    jQuery(this.elems.links).each(function() {
      smoothScroll.scroll(this);
    })
  }
}

var ToggleNav = function() {
  this.elems = {
    navLink: '#menu-primary a',
    navLinkItem: '#menu-primary>li',
    header: '#header',
    footer: '#footer',
    sections: 'section:not("#intro"):not("#start"):not(#impressum):not(#datenschutz)',
  },
  this.states = {
    navActive: 'current-menu-item',
  },
  this.getSectionsScrollTop = function() {
    var scrollTopSections = [],
        section;

    jQuery(this.elems.sections).each(function() {
      section = {
        top: Math.floor(jQuery(this).offset().top -1),
        bottom: Math.floor(jQuery(this).offset().top + jQuery(this).outerHeight() -1),
        offset: function() {
          if (jQuery(window).width() <= 500) {
            return Math.floor(jQuery(window).outerHeight() * .5);
          } else {
            return Math.floor(jQuery(window).outerHeight() * .65);
          }
        },
        hash: '#' + jQuery(this).attr('id')
      };

      scrollTopSections.push(section);
    });

    return scrollTopSections;
  },
  this.getCurrentSection = function(scrollTop) {
    var scrollTopSections = this.getSectionsScrollTop();

    for (var i = 0; i < scrollTopSections.length; i++) {
      if (scrollTop >= scrollTopSections[i].top && scrollTop <= scrollTopSections[i].bottom) {
        return scrollTopSections[i].hash;
      }
    }
  },
  this.getVisibleSection = function(scrollTop) {
    var scrollBottom = scrollTop + jQuery(window).height(),
        scrollTopSections = this.getSectionsScrollTop();

    for (var i = 0; i < scrollTopSections.length; i++) {
      if (scrollBottom >= (scrollTopSections[i].top + scrollTopSections[i].offset()) && scrollBottom <= (scrollTopSections[i].bottom + scrollTopSections[i].offset())) {
        return scrollTopSections[i].hash;
      }
    }
  },
  this.toggleNav = function(scrollTop) {
    var toggleNav = this,
        currentSection = this.getCurrentSection(scrollTop);

    this.resetNav();

    if (jQuery(this.elems.navLink + '[href="' + currentSection + '"]').parents('.sub-menu').length) {
      jQuery(this.elems.navLink + '[href="' + currentSection + '"]').parents('.menu-item-has-children').addClass(this.states.navActive);
    } else {
      jQuery(this.elems.navLink + '[href="' + currentSection + '"]').parent().addClass(this.states.navActive);
    }
  },
  this.resetNav = function() {
    jQuery(this.elems.navLink).each(function() {
      jQuery(this).parent().removeClass(toggleNav.states.navActive);
    });
  },
  this.init = function() {
    var toggleNav = this,
        scrollTop = Math.floor(jQuery(window).scrollTop());

    this.toggleNav(scrollTop);

    jQuery(window).on('scroll resize', function() {
      scrollTop = Math.floor(jQuery(window).scrollTop());

      toggleNav.toggleNav(scrollTop);

      if (Math.floor(jQuery(document).outerHeight()) == Math.floor(scrollTop + jQuery(window).height())) {
        toggleNav.resetNav();
      }
    });
  }
}

var ToggleSections = function() {
  this.toggleNav = new ToggleNav(),
  this.states = {
    sectionActive: 'active',
    sectionVisible: 'visible'
  },
  this.toggleSections = function(scrollTop) {
    var toggleSections = this,
        currentSection = this.toggleNav.getCurrentSection(scrollTop);

    jQuery(this.toggleNav.elems.sections).each(function() {
      jQuery(this).removeClass(toggleSections.states.sectionActive);
    })
    jQuery(this.toggleNav.elems.sections + currentSection).addClass(this.states.sectionActive);
  },
  this.toggleVisibleSections = function(scrollTop) {
    var toggleSections = this,
        visibleSection = this.toggleNav.getVisibleSection(scrollTop);

    jQuery(this.toggleNav.elems.sections).each(function() {
      jQuery(this).removeClass(toggleSections.states.sectionVisible);
    })
    jQuery(this.toggleNav.elems.sections + visibleSection).addClass(this.states.sectionVisible);
  },
  this.init = function() {
    var toggleSections = this,
        scrollTop = jQuery(window).scrollTop();

    this.toggleSections(scrollTop);
    this.toggleVisibleSections(scrollTop);

    jQuery(window).on('scroll resize', function() {
      scrollTop = jQuery(window).scrollTop();

      toggleSections.toggleSections(scrollTop);
      toggleSections.toggleVisibleSections(scrollTop);
    });
  }
}

var BackToTopScroll = function() {
  this.introAnim = new IntroAnim(),
  this.elems = {
    link: '[href*="#start"]'
  },
  this.hash = '#start',
  this.init = function() {
    var backToTopScroll = this,
        elemBottom = this.introAnim.getBottom(this.introAnim.elems.scroll.toElem);

    jQuery(this.elems.link).on('click', function(e) {
      e.preventDefault();
      if (jQuery(window).scrollTop() > 0) {
        if (jQuery(window).width() <= backToTopScroll.introAnim.breakpoints.xs) {
          backToTopScroll.introAnim.scroll(0, 0);
          setTimeout(function() {
            window.location.hash = backToTopScroll.hash;
          }, 100)
        } else {
          backToTopScroll.introAnim.scroll(elemBottom, 0);
        }
      }
    });
  }
}

var ToggleModals = function() {
  this.elems = {
    triggers: '#menu-footer a[target="_blank"]',
    modals: '.modal',
    body: 'body',
    wrapper: '#modals',
    page: 'html, body'
  },
  this.states = {
    navActive: 'current-menu-item',
    active: 'active',
    bodyActive: 'modal-active',
    disableCloseByScroll: 'no-scroll-close'
  },
  this.vars = {
    scrollModalOffset: jQuery(window).height() * .3333333
  },
  this.options = {
    delay: 0,
    duration: 800,
    easing: 'easeOutCubic'
  },
  this.resetModals = function(event) {
    var toggleModals = this,
        offset = jQuery(document).outerHeight() - jQuery(window).height() - jQuery(this.elems.wrapper).outerHeight() + this.vars.scrollModalOffset;
        offset = Math.floor(offset);

    if (event.type == 'click') {
      jQuery(this.elems.page).animate({
        scrollTop: offset
      }, this.options.duration, this.options.easing, function() {
      });
    }

    jQuery(toggleModals.elems.body).removeClass(toggleModals.states.bodyActive);
    jQuery(toggleModals.elems.wrapper).css('height', 0);

    setTimeout(function() {
      jQuery(toggleModals.elems.triggers).each(function() {
        jQuery(this).parent().removeClass(toggleModals.states.navActive);
      });
      jQuery(toggleModals.elems.modals).each(function() {
        jQuery(this).removeClass(toggleModals.states.active);
      });
    }, this.options.duration)
  },
  this.activateModal = function(trigger) {
    var toggleModals = this,
        target = jQuery(trigger).attr('href'),
        trigger = jQuery(trigger).parent(),
        height,
        currentScroll = jQuery(window).scrollTop(),
        offset = currentScroll + this.vars.scrollModalOffset;

    jQuery(trigger).addClass(this.states.navActive);
    jQuery(target).addClass(this.states.active);

    window.location.hash = target;
    jQuery(this.elems.body).addClass(this.states.bodyActive)
                           .addClass(this.states.disableCloseByScroll);
    height = jQuery(target).outerHeight();
    jQuery(this.elems.wrapper).css('height', height);

    jQuery(this.elems.page).animate({
      scrollTop: offset
    }, this.options.duration, this.options.easing, function() {
      jQuery(toggleModals.elems.body).removeClass(toggleModals.states.disableCloseByScroll);
    });
  },
  this.init = function() {
    var toggleModals = this,
        modals,
        trigger,
        triggerPos,
        scrollTop;

    jQuery(document).on('click touchend', function(e) {
      trigger = jQuery(e.target).closest(toggleModals.elems.triggers);
      modals = jQuery(e.target).closest(toggleModals.elems.modals);

      if (trigger.length) {
        e.preventDefault();
        toggleModals.activateModal(trigger);
      }

      if (!trigger.length && !modals.length && jQuery(toggleModals.elems.body).hasClass(toggleModals.states.bodyActive)) {
        toggleModals.resetModals(e);
      }
    })

    jQuery(window).on('scroll', function(e) {
      triggerPos = jQuery(document).outerHeight() - jQuery(window).height() - jQuery(toggleModals.elems.wrapper).outerHeight() + toggleModals.vars.scrollModalOffset;
      scrollTop = jQuery(window).scrollTop();


      if (scrollTop <= triggerPos && !jQuery(toggleModals.elems.body).hasClass(toggleModals.states.disableCloseByScroll) && jQuery(toggleModals.elems.body).hasClass(toggleModals.states.bodyActive)) {
        toggleModals.resetModals(e);
      }
    })
  }
}

// Obj
var introAnim = new IntroAnim();
var tabNavProducts = new TabNavProducts();
var videoAutoplay = new VideoAutoplay();
var animationStates = new AnimationStates();
var stickyNav = new StickyNav();
var smoothScroll = new SmoothScroll();
var toggleNav = new ToggleNav();
var toggleSections = new ToggleSections();
var backToTopScroll = new BackToTopScroll();
var toggleModals = new ToggleModals();

// DOM
jQuery(document).ready(function() {
  introAnim.init();
  tabNavProducts.init();
  videoAutoplay.init();
  animationStates.init();
  stickyNav.init();
  smoothScroll.init();
  toggleNav.init();
  toggleSections.init();
  backToTopScroll.init();
  toggleModals.init();
})
