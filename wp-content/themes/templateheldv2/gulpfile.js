// Requires
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyJS = require('gulp-uglify');
var minifyCSS = require('gulp-cssnano');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var autoprefixer = require('autoprefixer');

// Variables
var themeDir = '',
    sassSrc = themeDir + 'sass/**/*.scss',
    cssDir = themeDir + '',
    cssSrc = cssDir + 'style.css',
    jsDir = themeDir + 'js',
    jsSrc = jsDir + '/*.js',
    jsMinDir = themeDir + 'js/min';

// Tasks development
// SASS compile
gulp.task('sass', function() {
  return gulp.src(sassSrc)
    .pipe(sass())  // Converts SASS to CSS via gulp-sass
    .pipe(gulp.dest(cssDir))
})

// Minify JS
gulp.task('js', function() {
  return gulp.src(jsSrc)
    .pipe(minifyJS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(jsMinDir))
})

// Minify CSS
gulp.task('css', function() {
  return gulp.src(cssSrc)
    .pipe(minifyCSS([autoprefixer()]))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(cssDir))
})

// Gulp watch
gulp.task('watch', function() {
  gulp.watch(sassSrc, ['sass']);
  // gulp.watch(cssSrc, ['css']);     // For development phase disabled, takes about 1s each time
  gulp.watch(jsSrc, ['js']);
})

// Default
gulp.task('default', function() {
  runSequence('sass', 'css', 'js', 'watch');
})

// Manual build
gulp.task('build', function() {
  runSequence('sass', 'css', 'js');
})
